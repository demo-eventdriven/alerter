package be.jschoreels.demo.eventdriven.services.alerter.repository;

import be.jschoreels.demo.eventdriven.services.alerter.domain.PriceAlertConfiguration;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PriceAlertConfigurationRepository extends CrudRepository<PriceAlertConfiguration, String> {

    List<PriceAlertConfiguration> findByItemId(String itemId);

}
