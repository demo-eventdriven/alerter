package be.jschoreels.demo.eventdriven.services.alerter.consumer;

import be.jschoreels.demo.eventdriven.services.alerter.api.event.AlertTriggered;
import be.jschoreels.demo.eventdriven.services.alerter.domain.PriceAlertConfiguration;
import be.jschoreels.demo.eventdriven.services.alerter.repository.PriceAlertConfigurationRepository;
import be.jschoreels.demo.eventdriven.services.pricetracking.api.event.ItemPriceDecreased;
import be.jschoreels.demo.eventdriven.services.pricetracking.api.event.ItemPriceIncreased;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Component;

import java.time.ZonedDateTime;
import java.util.List;

@Component
public class Receiver {

    public static Logger logger = LoggerFactory.getLogger(Receiver.class);

    private final JmsTemplate jmsTemplate;

    private final PriceAlertConfigurationRepository priceAlertConfigurationRepository;

    @Autowired
    public Receiver(JmsTemplate jmsTemplate, PriceAlertConfigurationRepository priceAlertConfigurationRepository) {
        this.jmsTemplate = jmsTemplate;
        this.priceAlertConfigurationRepository = priceAlertConfigurationRepository;
    }


    @JmsListener(destination = "Consumer.Alerter:AlertTriggered.VirtualTopic.Price:ItemPriceDecreased", containerFactory = "queueListenerFactory")
    public void receiveMessageItemPriceDecreased(ItemPriceDecreased itemPriceDecreased) {
        logger.info("Received <" + itemPriceDecreased.toString() + ">");
        List<PriceAlertConfiguration> priceAlertConfigurationRepositoryByItemId =
                priceAlertConfigurationRepository.findByItemId(itemPriceDecreased.getItem());

        priceAlertConfigurationRepositoryByItemId.stream()
                .filter(configuration -> configuration.getThresholdPrice().compareTo(itemPriceDecreased.getNewPrice()) >= 0)
                .peek(configuration -> logger.info("Alert matched for user {} for item {}", configuration.getUserId(), configuration.getItemId()))
                .forEach(configuration ->
                        jmsTemplate.convertAndSend("VirtualTopic.Alert:AlertTriggered",
                                AlertTriggered.newBuilder()
                                    .withUser(configuration.getUserId())
                                    .withRootEventPayload(itemPriceDecreased)
                                    .withTriggeredTime(ZonedDateTime.now())
                                    .build()
                        )
                );
    }
}