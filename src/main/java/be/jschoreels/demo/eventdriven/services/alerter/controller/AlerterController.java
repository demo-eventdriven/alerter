package be.jschoreels.demo.eventdriven.services.alerter.controller;

import be.jschoreels.demo.eventdriven.services.alerter.api.event.AlertCreated;
import be.jschoreels.demo.eventdriven.services.alerter.consumer.Receiver;
import be.jschoreels.demo.eventdriven.services.alerter.domain.PriceAlertConfiguration;
import be.jschoreels.demo.eventdriven.services.alerter.repository.PriceAlertConfigurationRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AlerterController {

    public static Logger logger = LoggerFactory.getLogger(Receiver.class);

    @Autowired
    private PriceAlertConfigurationRepository priceAlertConfigurationRepository;

    @Autowired
    private JmsTemplate jmsTemplate;

    @RequestMapping(value = "/alert", method = RequestMethod.POST)
    public void addAlertRule(@RequestBody  PriceAlertConfiguration priceAlertConfiguration){
        PriceAlertConfiguration savedConfiguration = priceAlertConfigurationRepository.save(
                priceAlertConfiguration
        );
        logger.info("Saved PriceAlertConfiguration : {}", priceAlertConfiguration.toString());
        AlertCreated alertCreated = AlertCreated.newBuilder()
                .withId(savedConfiguration.getId())
                .withItemId(savedConfiguration.getItemId())
                .withUserId(savedConfiguration.getUserId())
                .withThresholdPrice(savedConfiguration.getThresholdPrice())
                .build();
        jmsTemplate.convertAndSend("VirtualTopic.Alert:AlertCreated",
                alertCreated);
        logger.info("Sent event AlertCreated : {}", alertCreated);
    }

}
