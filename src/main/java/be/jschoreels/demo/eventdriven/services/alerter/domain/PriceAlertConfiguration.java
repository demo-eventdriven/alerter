package be.jschoreels.demo.eventdriven.services.alerter.domain;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Access(AccessType.FIELD)
public class PriceAlertConfiguration {

    @Id
    @GeneratedValue(generator="system-uuid")
    @GenericGenerator(name="system-uuid",
            strategy = "uuid")
    private String id;
    private String userId;

    private String itemId;

    private BigDecimal thresholdPrice;

    public PriceAlertConfiguration(){}

    public PriceAlertConfiguration(String id, String userId, String itemId, BigDecimal thresholdPrice) {
        this.id = id;
        this.userId = userId;
        this.itemId = itemId;
        this.thresholdPrice = thresholdPrice;
    }

    public String getId() {
        return id;
    }

    public String getUserId() {
        return userId;
    }

    public String getItemId() {
        return itemId;
    }

    public BigDecimal getThresholdPrice() {
        return thresholdPrice;
    }

    @Override
    public String toString() {
        return "PriceAlertConfiguration{" +
                "id='" + id + '\'' +
                ", userId='" + userId + '\'' +
                ", itemId='" + itemId + '\'' +
                ", thresholdPrice=" + thresholdPrice +
                '}';
    }
}
